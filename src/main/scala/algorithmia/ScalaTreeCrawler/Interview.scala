package algorithmia.ScalaTreeCrawler

import org.json4s._
import org.json4s.native.JsonMethods._

case class Interview(
  children: List[String],
  reward: Float
)

object Interview {

  // Brings in default date formats, etc.
  implicit val formats = DefaultFormats

  def fromJson(payload: String): Interview =
    parse(payload).extract[Interview]

}