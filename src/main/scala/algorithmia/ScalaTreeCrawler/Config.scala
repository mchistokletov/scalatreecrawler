package algorithmia.ScalaTreeCrawler

import scala.concurrent.duration._

object Config {
  val connTimeoutMs: Int = 10000
  val readTimeoutMs: Int = 10000
  val executionTimeout: FiniteDuration = 1 minute
}
