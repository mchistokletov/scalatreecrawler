package algorithmia.ScalaTreeCrawler

import scala.concurrent.{Await, Future}
import scalaj.http._

/*
* This is the entry point.
* Arg entered into Algorithmia Console will be passed in here.
**/
class ScalaTreeCrawler {

  def apply(url: String): Float = {
    ScalaTreeCrawler.crawl(url)
  }

}

/*
* Caveat:
* If the tree isn't actually a tree and has a loop, or is a very deep tree,
* it will run until it either runs out of time or memory.
*
* General idea:
*                                                            / traverseTree() -> ...
* traverseTree() -> asyncHttpRequest() -> processResponse() -> traverseTree() -> ...
*                                                            \ traverseTree() -> ...
**/
object ScalaTreeCrawler {
  import scala.concurrent.ExecutionContext.Implicits.global

  type Url = String

  def crawl(initUrl: Url): Float = {
    val futureResult = traverseTree(initUrl)

    Await.result(futureResult, Config.executionTimeout)
  }


  private def traverseTree(url: Url): Future[Float] = {
    asyncHttpRequest(url).flatMap(processResponse)
  }


  private def asyncHttpRequest(url: Url): Future[HttpResponse[String]] =
    Future {
      Http(url).timeout(Config.connTimeoutMs, Config.readTimeoutMs).asString
    }


  private def processResponse(response: HttpResponse[String]): Future[Float] = {
    response.code match  {
      case 200 => {
        val interview = Interview.fromJson(response.body)
        val recursiveSteps: List[Future[Float]] = interview.children.map(traverseTree)

        Future.sequence(recursiveSteps)   // List[Future[_]] -> Future[List[_]]
          .map(_.sum)                     // Sum up rewards from recursive steps
          .map(_ + interview.reward)      // Add the reward from the current node
      }

      case x => throw new Exception(s"Http request returned $x code; Response object: $response")
    }
  }

}
