package algorithmia.ScalaTreeCrawler

import org.json4s.MappingException
import org.scalatest.{FlatSpec, Matchers}

class InterviewSpec extends FlatSpec with Matchers {

  "Interview deserializer" should "be able to create case class from json" in {
    val json = """ {"children":["http://algo.work/interview/b","http://algo.work/interview/c"],"reward":1} """
    val interview = Interview.fromJson(json)

    assert(interview.children.contains("http://algo.work/interview/b"))
    assert(interview.children.contains("http://algo.work/interview/c"))
    assert(interview.reward == 1f)
  }

  "Interview deserializer" should "be able to create case class even no children are provided" in {
    val json = """ {"reward":1} """
    val interview = Interview.fromJson(json)

    assert(interview.children.isEmpty)
    assert(interview.reward == 1f)
  }

  "Interview deserializer" should "throw MappingException when children array is malformed" in {
    val json = """ {"children": "not array","reward":1} """

    assertThrows[MappingException] {
      Interview.fromJson(json)
    }

  }

  "Interview deserializer" should "throw MappingException when reward is missing" in {
    val json = """ {"children":["http://algo.work/interview/b"]} """

    assertThrows[MappingException] {
      Interview.fromJson(json)
    }
  }


}
